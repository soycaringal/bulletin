<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Post;
use App\Setting;
use App\Rating;
use App\View;
use App\User;

Auth::routes();

Route::get('/', function() {

	$filter['flag'] = 1; 

    $posts = Post::where($filter);
    if(isset(\Auth::user()->type) && \Auth::user()->type != 'admin') {
        $posts->where('bulletin', 'like', '%'.\Auth::user()->type.'%');
    }

	$posts = $posts->get();

	foreach ($posts as $key => $post) {
		if(isset($post->files[0]->filename)) {
			$posts[$key]['filename'] = $post->files[0]->filename;
		} else {
			$posts[$key]['filename'] = '';
		}

		$ratings = Rating::where('post_id', $post->id)->get();

		$aveRating = 0;

		if(!$ratings->isEmpty()) {
			$rate = [
				5 => 0,
				4 => 0,
				3 => 0,
				2 => 0,
				1 => 0
				];
			foreach ($ratings as $value) {
				$rate[$value['rating']]++; 
			}


			$totalWeight = 0;
			$totalReviews = 0;

			foreach ($rate as $weight => $numberofReviews) {
			    $WeightMultipliedByNumber = $weight * $numberofReviews;
			    $totalWeight += $WeightMultipliedByNumber;
			    $totalReviews += $numberofReviews;
			}

			//divide the total weight by total number of reviews
			$aveRating = $totalWeight / $totalReviews;

			$aveRating = floor($aveRating);
		}

		$posts[$key]['aveRating'] = $aveRating;

		 $authId = null;
        if(isset(Auth::user()->id)) {
            $authId = Auth::user()->id;
        } 

        $posts[$key]['authId'] = $authId;

        $limit = 250;

         $string = strip_tags($post['desc']);

        if (strlen($string) > $limit) {
            // truncate string
            $stringCut = substr($string, 0, $limit);

            // make sure it ends in a word so assassinate doesn't become ass...
            $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="/post/'.$post['id'].'">Read More</a>';
        }

        $posts[$key]['desc'] = $string;

	}

    $bg = Setting::where('type', 'bg')->first();
    $font = Setting::where('type', 'font')->first();


    return view('index', ['posts' => $posts, 'bg' => $bg, 'font' => $font]);
});

Route::get('/home', 'PostsController@index');
Route::get('/post/{id}', function($id) {
	$post = Post::where('id', $id)->first();

	$view = new View();
	$view->post_id = $id;

	$view->save();

	$ratings = Rating::where('post_id', $id)->get();

	$aveRating = 0;

	if(!$ratings->isEmpty()) {
		$rate = [
			5 => 0,
			4 => 0,
			3 => 0,
			2 => 0,
			1 => 0
			];
		foreach ($ratings as $key => $value) {
			$rate[$value['rating']]++; 
		}


		$totalWeight = 0;
		$totalReviews = 0;

		foreach ($rate as $weight => $numberofReviews) {
		    $WeightMultipliedByNumber = $weight * $numberofReviews;
		    $totalWeight += $WeightMultipliedByNumber;
		    $totalReviews += $numberofReviews;
		}

		//divide the total weight by total number of reviews
		$aveRating = $totalWeight / $totalReviews;

		$aveRating = floor($aveRating);
	}

    $template = Setting::where('type', 'template')->first();

  $bg = Setting::where('type', 'bg')->first();
    $font = Setting::where('type', 'font')->first();


    return view('post-detail', ['post' => $post, 'aveRating' => $aveRating, 'template' => $template, 'bg' => $bg, 'font' => $font]);
});

Route::get('/admin', 'AdminController@index');
Route::resource('/admin/users','AdminUsersController');
Route::resource('/admin/posts', 'PostsController');
Route::get('/admin/view', 'PostsController@view');
Route::get('/admin/rating', 'PostsController@rating');
Route::get('/admin/bulletin', 'PostsController@bulletin');

Route::get('/admin/users', [
	'uses' => 'AdminUsersController@index',
	'as' => 'admin',
	'middleware' => 'roles',
	'roles' => ['admin']
]);	

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/api/posts', 'ApiPostsController@index');
Route::post('/api/rating/create', 'ApiPostsController@rating');
Route::get('/api/views', 'ApiPostsController@views');
Route::get('/admin/settings', 'SettingController@index');
Route::get('/admin/settings/bg', 'SettingController@bg');
Route::resource('/admin/settings', 'SettingController');
Route::get('/api/settings', 'ApiPostsController@template');
Route::get('user/{id}/delete', ['as' => 'users.delete', 'uses' => 'AdminUsersController@destroy']);

Route::get('/api/posts/preview', 'ApiPostsController@preview');

Route::get('/ajax/users', 'AdminUsersController@indexApi');
Route::get('/preview', function() {

	if(isset($_GET['type'])) {

		if($_GET['type'] == 'general') {
            $posts = Post::orderBy('id', 'DESC')->get();

		}  else {
            $posts = Post::where('bulletin', 'like', '%'.$_GET['type'].'%')
            ->orderBy('id', 'DESC')->get();
			
		}
   } else if(isset($_GET['q']) && !empty($_GET['q'])) { 
        $posts = new Post();

        $posts = $posts->where('author', 'LIKE', "%" . $_GET['q'] . "%")
                ->orWhere('title', 'LIKE', "%" . $_GET['q'] . "%")
                ->orWhere('type', 'LIKE', "%" . $_GET['q'] . "%")
                ->orWhere('bulletin', 'LIKE', "%" . $_GET['q'] . "%")
                ->orWhere('desc', 'LIKE', "%" . $_GET['q'] . "%")->get();
    }


	// foreach ($posts as $key => $post) {
	// 	if(isset($post->files[0]->filename)) {
	// 		$posts[$key]['filename'] = $post->files[0]->filename;
	// 	} else {
	// 		$posts[$key]['filename'] = '';
	// 	}

	// 	$ratings = Rating::where('post_id', $post->id)->get();

	// 	$aveRating = 0;

	// 	if(!$ratings->isEmpty()) {
	// 		$rate = [
	// 			5 => 0,
	// 			4 => 0,
	// 			3 => 0,
	// 			2 => 0,
	// 			1 => 0
	// 			];
	// 		foreach ($ratings as $key => $value) {
	// 			$rate[$value['rating']]++; 
	// 		}


	// 		$totalWeight = 0;
	// 		$totalReviews = 0;

	// 		foreach ($rate as $weight => $numberofReviews) {
	// 		    $WeightMultipliedByNumber = $weight * $numberofReviews;
	// 		    $totalWeight += $WeightMultipliedByNumber;
	// 		    $totalReviews += $numberofReviews;
	// 		}

	// 		//divide the total weight by total number of reviews
	// 		$aveRating = $totalWeight / $totalReviews;
	// 	}

	// 	$posts[$key]['aveRating'] = $aveRating;

	// 	 $authId = null;
 //        if(isset(Auth::user()->id)) {
 //            $authId = Auth::user()->id;
 //        } 

 //        $posts[$key]['authId'] = $authId;
	// }
	
    $bg = Setting::where('type', 'bg')->first();
    $font = Setting::where('type', 'font')->first();

    return view('preview', ['posts' => $posts, 'bg' => $bg, 'font' => $font]);
});