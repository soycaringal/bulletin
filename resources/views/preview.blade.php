<!DOCTYPE html>
<html lang="en" class="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ASIATECH CBRBS - Bulletin</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">
        <link href="/css/sb-admin.css" rel="stylesheet">
        <link href="/assets/fonts/font-awesome.min.css" rel="stylesheet">
        <link href="/css/custom.css" rel="stylesheet">
        
        <script src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/angular.min.js')}}"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="{{asset('js/custom.js')}}"></script>
        <script src="{{asset('js/doon.js')}}"></script>

  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">


<style>

    .template-1 {
        background: url('/images/{{$bg->name}}') no-repeat;
        background-size: cover;
        font-family: {{$font->name}};
    }
</style>
    </head>

    <body>
        <div class=" template">

        @if (!Auth::check()) 
        <a href="/login" class="pull-right" style="margin-right: 20px; font-size: 16px; color: white;"><i class="fa fa-sign-in"></i> Login</a>
        @endif

        <a href="/admin" class="pull-right" style="margin-right: 20px; font-size: 16px; color: white;"><i class="fa fa-home"></i> Dasboard</a>
        <div class="container">
            <div class="row">
<!--                  @foreach ($posts as $post)

                     <div class="col-lg-8 col-md-10 mx-auto">
                          <div class="post-preview">
                            <a href="/post/{{ $post->id }}">
                                @if (!$post->files->isEmpty())
                                    <img width="100%" src="/images/{{ $post->files{0}->filename }}" alt=""></td>
                                @endif
                              <h2 class="post-title">
                               {{ $post->title }}
                              </h2>
                            </a>
                              <p class="post-subtitle">
                                {{ $post->desc }}
                              </p>
                            <p class="post-meta">Posted by
                              <a href="#">{{ $post->author }}</a>
                              on {{ $post->created_at }}</p>
                          </div>
                          <hr>
                          </div>
         
                @endforeach
            </div> -->

            <div ng-app="myApp" ng-controller="myCtrl" ng-cloak>
                <h1></h1>

                <div class="temp-1">
          <div class="row" >
                    @foreach ($posts as $post)

                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="/images/pin.png" width="75"  class="pin" alt="">

                                <h5 class="text-center">{{ $post->type }}</h5>
                                
                                <a href="#" id="pop2" ng-click="myFunc()">
                                @if (isset($post->files[0]))
                                 <img  id="imageresource"  width="100%" src="/images/{{ $post->files[0]->filename ? $post->files[0]->filename : '' }}" alt="">
                                @endif
                               
                                </a>
                                

                                <div class="caption">
                                    <a href="/post/{{ $post->id }}">
                                        <h3 class="title">{{ $post->title }}</h3>
                                    </a>
    
                                @if (!empty($post->video))

                                     <a href="#" class="btn btn-info" data-toggle="modal" onclick="showModal(this)" data-id="" data-target="#videoModal" data-video="{{ $post->video }}">Play Video</a>
                                @endif
                                    <div class="focus rating-group">
                                    <label class="pull-left"></label>
                                    <div class="rating-field" data-do="rating-field" ng-init="rate($event)" my-dir>

                                         @if ($post->aveRating == '0')
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($post->aveRating == 1)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($post->aveRating == 2)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($post->aveRating == 3)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($post->aveRating == 4)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($post->aveRating == 5)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        @endif
                                       
                                         <input type="hidden" name="rating" data-do="rating-vote" data-on="change" value="" />
                                        <input type="hidden" name="post_id" value="{{ $post->id}}" />
                                        <input type="hidden" name="profile_id" value="@if (isset(Auth::user()->id)){{ Auth::user()->id }}@endif">
                                    </div>
                                </div>

                                    <i>{{ $post->author}}</i><br>
                                    <i class="date" > {{ $post->created_at}}</i> <br><br>  
                                    <p class="desc"> {!!$post->desc!!}</p>
                                </div>
                                
                            </div>
                        </div>

                    @endforeach
                    </div>
                </div>
                <div class="temp-2">
                    
                    @foreach ($posts as $post)

                     <div class="col-lg-8 col-md-10 mx-auto">
                          <div class="post-preview">
                            <a href="/post/{{ $post->id }}">
                                @if (!$post->files->isEmpty())
                                    <img width="100%" src="/images/{{ $post->files{0}->filename }}" alt=""></td>
                                @endif
                              <h2 class="post-title">
                               {{ $post->title }}
                              </h2>
                            </a>
                              <p class="post-subtitle">
                                {!!$post->desc!!}
                              </p>
                            <p class="post-meta">Posted by
                              <a href="#">{{ $post->author }}</a>
                              on {{ $post->created_at }}</p>
                          </div>
                        <div class="focus rating-group">
                                    <label class="pull-left"></label>
                                    <div class="rating-field" data-do="rating-field">
                                        @if ($post->aveRating == '0')
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($post->aveRating == 1)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($post->aveRating == 2)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($post->aveRating == 3)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($post->aveRating == 4)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($post->aveRating == 5)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        @endif
                                        <input type="hidden" name="rating" data-do="rating-vote" data-on="change" value="" />
                                        <input type="hidden" name="post_id" value="{{ $post->id}}" />
                                        <input type="hidden" name="profile_id" value="@if (isset(Auth::user()->id)){{ Auth::user()->id }}@endif">
                                    </div>
                                </div>
                          <hr>
                          </div>
         

                    @endforeach
                </div>
            </div>
        </div>
        
    </body>

<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div>
                    <iframe width="100%" height="350" src=""></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
 <script>

        var app = angular.module('myApp', []);
        app.service('getPostService', function($http) {
            this.getPostResult = function() {
                return $http({
                    method: 'GET',
                    url: '/api/posts'
                });
            };
        });

        app.run(function($rootScope) {
            $rootScope.posts = [];
        });

        app.controller("myCtrl", function($scope, $sce, getPostService) {
            $scope.posts = [];
                getPostService.getPostResult().then(function(result) {
                var posts = result.data.posts;
                var template = result.data.posts.template;
                    $scope.template = template;
                    $('body').addClass('template-' + template.flag);
                    $('.temp-' + template.flag).show();
               
                delete result.data.posts.template;

                angular.forEach(posts, function(row) {
                    $scope.posts.push({
                    "id": row.id,
                    "title": row.title,
                    "author": row.author,
                    "aveRating": row.aveRating,
                    "authId": row.authId,
                    "created_at": row.created_at,
                    "desc": $sce.trustAsHtml(row.desc),
                    "files": row.files,
                    "type": row.type,
                    "video": $sce.trustAsResourceUrl(row.video),
                    });
                });
            });

            $scope.myFunc = function() {
                $('#imagepreview').attr('src', $('#imageresource').attr('src')); // here asign the image to the modal when the user click the enlarge link
               $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
            };

            $scope.showModal = function(e) {

                console.log(e);
                  var trigger = $("body").find('[data-toggle="modal"]');

                  videoSRC = $(e.target).attr('video');
                  videoSRCauto = videoSRC + "?autoplay=1";

                $('#videoModal iframe').attr('src', videoSRCauto);
            };

            
        });

       
    function vote(rating, postId, profileId) {
        
        data = {
            'rating': rating,
            'post_id': postId,
            'profile_id': profileId,
        };

        $.post('/api/rating/create', data, function(result) {

            if(result.error) {
                toastr.error(result.message)
                return;
            }

            toastr.success('Successfuly voted!')
        });           
    }

         function showModal(e) {
                          var trigger = $("body").find('[data-toggle="modal"]');

                          videoSRC = $(e).attr('data-video');
                          videoSRCauto = videoSRC + "?autoplay=1";

                        $('#videoModal iframe').attr('src', videoSRCauto);
         }

    </script>
</html>
