@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ isset ($_GET['type']) ? ucwords($_GET['type']) : ''}} Post Management</h1>

         <form class="pull-right input-group col-md-3">
            <input type="text" id="search" class="form-control" name="q" placeholder="Search" value="{{isset ($_GET['q']) ? $_GET['q'] : ''}}">
            <span class="input-group-btn">
                <input class="btn btn-info" type="submit" value="Submit">
            </span>
        </form>
    </div>
</div>

<div class="panel-body">
    <a href="/admin/posts/create" class="btn btn-success">Create</a>

    <table class="table table-hover " style="table-layout: fixed;">
        <thead>
        <tr>
            <th>Post ID</th>
            <th>Title</th>
            <th>Author</th>
            <th class="col-sm-2">Description</th>
            <th>Type</th>
            <th>Bulletin</th>
            <th>Published</th>
            <th class="col-sm-3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($posts as $post)
        <tr>
            <td class="col-sm-2">{{ $post->id }}</td>
            <td class="col-sm-2">{{ $post->title }}</td>
            <td class="col-sm-2"><i>{{ $post->alias }}</i>{{ $post->author }}</td>
            <td class="col-sm-4">@if (!$post->files->isEmpty())
        <a href="#" id="pop">
    <img id="imageresource" src="/images/{{ $post->files{0}->filename }}"   width="100">
                @endif
     
</a>
{!! \Illuminate\Support\Str::words($post->desc, 20,'....')  !!}
                </td>
            <td>{{ $post->type }}</td>
            <td>{{ ucfirst(trans($post->bulletin)) }}</td>
            <td>{{ $post->flag == 1 ? 'Yes' : 'No' }}</td>
            <td >
                @if (Auth::user()->type == 'admin') 
                    {!! Form::open(['method' => 'PUT','route' => ['posts.update', $post->id], 'style' => 'display:inline']) !!}
                        {{ csrf_field() }}

                        @if ($post->flag == 1)
                        {!! Form::hidden('flag', 0) !!}
                        {!! Form::submit('Unpublish', ['class' => 'btn btn-info']) !!}
                        @else
                        {!! Form::hidden('flag', 1) !!}
                        {!! Form::submit('Publish', ['class' => 'btn btn-info']) !!}
                        @endif
                    {!! Form::close() !!}
                @endif
                <a href="{{route('posts.edit', $post->id) }}" class="btn btn-warning">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['posts.destroy', $post->id], 'style' => 'display:inline']) !!}
                {{ csrf_field() }}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>

        
   
        @endforeach
        </tbody>
    </table>
</div>

<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">
        <img src="" id="imagepreview" style="width: 100%; " >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
 
$('#search').on('keyup',function(){
 
value=$(this).val();
 console.log(value);
$.ajax({
 
type : 'get',
 
url : '/ajax/users',
 
data:{'q':value},
 
success:function(data){
$('tbody').html(data);
 
 }
 
});
 
 
 
})
 
</script>

<script>
    $("#pop").on("click", function() {
   $('#imagepreview').attr('src', $('#imageresource').attr('src')); // here asign the image to the modal when the user click the enlarge link
   $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});
</script>
@endsection
