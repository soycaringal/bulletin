@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Bulletin's Preview Management | {{ isset ($_GET['type']) ? ucwords($_GET['type']) : 'General'}}</h1>
    </div>
</div>

<div class="panel-body">

    <iframe src="/preview?type=<?php echo isset($_GET['type']) ? $_GET['type']: 'all'?>" frameborder="0" width="100%" height="1500"></iframe>
</div>
@endsection
