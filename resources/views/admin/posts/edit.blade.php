@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Posts</h1>
        </div>
    </div>

    <div class="panel-body">
        {!!
       Form::model($post, ['route' => ['posts.update', $post->id],
      'method' => 'put',
      'class' => 'form-horizontal',
      'files' => true]) !!}
    
        <div class="form-group">
            {!! Form::label('author', 'Author', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('author', $post->author , ['class' => 'form-control', 'placeholder' => 'Author']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('title', $post->title , ['class' => 'form-control', 'placeholder' => 'Title']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('title', 'Description', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::textarea('desc', $post->desc , ['class' => 'form-control', 'placeholder' => 'Description', 'required' => 'required', 'id' => 'summernote']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('title', 'Type', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                <select class="form-control" name="type">
                    <option value="News" {{$post->type == 'News' ? 'selected': ''}}>News</option>
                    <option value="Announcement" {{$post->type == 'Announcement' ? 'selected': ''}}>Announcement</option>
                    <option value="Post" {{$post->type == 'Post' ? 'selected': ''}}>Post</option>
                </select>
            </div>
        </div>
        
        <div class="form-group{ clearfix">
            <label class="control-label col-sm-3">Bulletin</label>
            <div class="col-sm-6">
                <div data-do="tag-field" class="tag-field">
                    @foreach ($post->bulletin as $bulletin)
                        <div class="tag">
                            <input type="text" class="tag-input text-field" name="bulletin[]" placeholder="Add Tag" value="{{$bulletin}}" />
                            <a class="remove" href="javascript:void(0)"><i class="fa fa-times"></i></a>
                        </div>
                    @endforeach
                </div>

                <div class="input-suggestion hide">
                    <ul class="suggestion-list"></ul>
                </div>

                <em><strong>Note: </strong>staff, faculty, student.</em>
               
            </div>
            <script id="tag-translations" type="text/json">Tag:Tag</script>
        </div>

        <div class="form-group">
            {!! Form::label('video', 'Video Url', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('video', $post->video , ['class' => 'form-control', 'placeholder' => 'https://www.youtube.com/watch?v=DP-ksbqk5ds']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('title', 'Images', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">

                {!! Form::file('files[]', ['class' => 'form-control', 'placeholder' => 'Content', 'multiple' => 'true']) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-6">
                @foreach ($post->files as $file)
                    <img width="100" src="/images/{{ $file->filename }}" alt=""></td>
                @endforeach
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-9 pull-right">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>

        {{ csrf_field() }}
        {!! Form::close() !!}
    </div>


<script type="text/javascript">
    $(document).ready(function() {
        $('#summernote').summernote({
          height:300,
        });
    });
</script>
@endsection

