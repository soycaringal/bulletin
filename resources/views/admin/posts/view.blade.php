@extends('layouts.admin')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Report Management | Total No. of Views</h1>

         <form class="pull-right input-group col-md-3">
            <input type="text" class="form-control" name="q" placeholder="Search" value="{{isset ($_GET['q']) ? $_GET['q'] : ''}}">
            <span class="input-group-btn" style="font-size: 10px !important;">
                <input class="btn btn-info" type="submit" value="Submit"> &nbsp;

                <a href="javascript:void(0);" onClick="window.print()" class="btn btn-success">Print</a>
            </span>
        </form>
    </div>
</div>
 <script type="text/javascript" src="/highcharts/highcharts.js"></script>
    @if ($posts->count() > 0)

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<div class="panel-body">
    <table class="table table-hover ">
        <thead>
        <tr>
            <th>View</th>
            <th>Image</th>
            <th>Title</th>
            <th>Author</th>
            <th>Type</th>
            <th>Bulletin</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($posts as $post)
        <tr>
            <td><h2>{{ $post->view}}</h2></td>
            <td>
                @if (!$post->files->isEmpty())
                <img width="100" src="/images/{{ $post->files{0}->filename }}" alt=""></td>
                @endif

            <td class="col-sm-2">{{ $post->title }}</td>
            <td class="col-sm-2">{{ $post->author }}</td>
            <td>{{ $post->type }}</td>
            <td>{{ ucfirst(trans($post->bulletin)) }}</td>
            
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
<h3><i>No result.</i></h3>
    @endif


<script>
 Highcharts.chart('container', {

    title: {
        text: 'Number of Times the Post Have Views Graph'
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Number of Viewer(s)'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: {{$startYear['year']}},
        }
    },

    series: [{
        name: 'Posts',
        data: [
        @foreach ($charts as $chart)
            {{$chart['total']}},
        @endforeach

        ]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 00
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
</script>
@endsection
