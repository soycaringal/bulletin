@extends('layouts.admin')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Report Management | Total No. of Views</h1>

         <form class="pull-right input-group col-md-3">
            <input type="text" class="form-control" name="q" placeholder="Search" value="{{isset ($_GET['q']) ? $_GET['q'] : ''}}">
            <span class="input-group-btn" style="font-size: 10px !important;">
                <input class="btn btn-info" type="submit" value="Submit"> &nbsp;

                <a href="javascript:void(0);" onClick="window.print()" class="btn btn-success">Print</a>
            </span>
        </form>
    </div>
</div>
 <script type="text/javascript" src="/highcharts/highcharts.js"></script>
    @if ($posts->count() > 0)

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<div class="panel-body">
    <table class="table table-hover ">
        <thead>
        <tr>
            <th>Title</th>
            <th>Author</th>
            <th>Type</th>
            <th>Bulletin</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($posts as $post)
        <tr>
            <td class="col-sm-2">{{ $post->title }}</td>
            <td class="col-sm-2">{{ $post->author }}</td>
            <td>{{ $post->type }}</td>
            <td>{{ ucfirst(trans($post->bulletin)) }}</td>
            
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
<h3><i>No result.</i></h3>
    @endif


<script>
 
// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Browser market shares. January, 2018'
    },
    subtitle: {
        text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total percent market share'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    "series": [
        {
            "name": "Ratings",
            "colorByPoint": false,
            "data": [

                {
                    "name": "1 Star",
                    "color": '#EA0000',
                    "y": {{$rating[1]}},
                    "drilldown": "1 Star"
                },
                {
                    "name": "2 Stars",
                    "y": {{$rating[2]}},
                    "color": "#FA9922",
                    "drilldown": "2 Sars"
                },
                {
                    "name": "3 Stars",
                    "y": {{$rating[3]}},
                    "color": "#fec923",
                    "drilldown": "3 Stars"
                },
                {
                    "name": "4 Stars",
                    "y": {{$rating[4]}},
                    "color": "#92d14f",
                    "drilldown": "3 Stars"
                },
                {
                    "name": "5 Stars",
                    "y": {{$rating[5]}},
                    "color": "#00602b",
                    "drilldown": "4 Stars"
                }
            ]
        }
    ]
});
</script>
@endsection
    