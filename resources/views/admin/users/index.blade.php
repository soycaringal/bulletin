@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Account Management</h1>

        <form class="pull-right input-group col-md-3">
            <input type="text" id="search"  class="form-control" name="q" placeholder="Search" value="{{isset ($_GET['q']) ? $_GET['q'] : ''}}">
        </form>
    </div>
</div>

<div class="panel-body">
    <a href="/admin/users/create" class="btn btn-success">Create</a>
    <table class="table table-hover ">
        <thead>
        <tr>
            <th>Registered ID N o.</th>
            <th>Fullname</th>
            <th>Alias of A.K.A</th>
            <th>Email</th>
            <th>Type</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->employee_id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->alias }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->type }}</td>
            <td>
                <a href="{{route('users.edit', $user->id) }}" class="btn btn-warning">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id], 'style' => 'display:inline']) !!}
                {{ csrf_field() }}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>

<script type="text/javascript">
 
$('#search').on('keyup',function(){
 
value=$(this).val();
 console.log(value);
$.ajax({
 
type : 'get',
 
url : '/ajax/users',
 
data:{'q':value},
 
success:function(data){
 console.log(data);
$('tbody').html(data);
 
 }
 
});
 
 
 
})
 
</script>

@endsection


