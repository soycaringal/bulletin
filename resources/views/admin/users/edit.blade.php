@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Account Management</h1>
        </div>
    </div>

    <div class="panel-body">
        {!!
       Form::model($user, ['route' => ['users.update', $user->id],
      'method' => 'put',
      'class' => 'form-horizontal',
      'files' => true]) !!}
    
        <div class="form-group">
            {!! Form::label('employee_id', 'Registered ID No.', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-3">
                {!! Form::text('employee_id', $user->employee_id , ['class' => 'form-control', 'placeholder' => 'Registered ID No.']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('name', 'Fullname', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-3">
                {!! Form::text('name', $user->name , ['class' => 'form-control', 'placeholder' => 'Fullname']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('name', 'Alias of A.K.A', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-3">
                {!! Form::text('alias', $user->alias , ['class' => 'form-control', 'placeholder' => 'Alias of A.K.A']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-3">
                {!! Form::email('email', $user->email , ['class' => 'form-control', 'placeholder' => 'Email']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('title', 'Type', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-3">
                <select class="form-control" name="type">
                    <option value="staff" {{$user->type == 'staff' ? 'selected': ''}}>Staff</option>
                    <option value="faculty" {{$user->type == 'faculty' ? 'selected': ''}}>Faculty</option>
                    <option value="student" {{$user->type == 'student' ? 'selected': ''}}>Student</option>
                    <option value="admin" {{$user->type == 'admin' ? 'selected': ''}}>Admin</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-9 pull-right">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>

        {{ csrf_field() }}
        {!! Form::close() !!}
    </div>
@endsection
