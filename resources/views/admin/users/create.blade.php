@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Account Management</h1>
        </div>
    </div>

    <div class="panel-body">
        {!! Form::open([
        'action' => 'AdminUsersController@store',
        'method' => 'post',
        'class' => 'form-horizontal',
        'files' => true]) !!}
        
         <div class="form-group">
            {!! Form::label('employee_id', 'Registered ID No.', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-3">
                {!! Form::text('employee_id', null , ['class' => 'form-control', 'placeholder' => 'Registered ID No.']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('name', 'Fullname', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-3">
                {!! Form::text('name', null , ['class' => 'form-control', 'placeholder' => 'Fullname']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('name', 'Alias of A.K.A.', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-3">
                {!! Form::text('alias', null , ['class' => 'form-control', 'placeholder' => 'Alias of A.K.A.']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-3">
                {!! Form::email('email', null , ['class' => 'form-control', 'placeholder' => 'Email']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('title', 'Type', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-3">
                <select class="form-control" name="type">
                    <option value="staff">Staff</option>
                    <option value="faculty">Faculty</option>
                    <option value="student">Student</option>
                    <option value="admin">Admin</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('password', 'Password', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-3">
                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
            </div>

        </div>

        <div class="form-group">
            {!! Form::label('confirm', 'Confirm', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-3">
                {!! Form::password('confirm', ['class' => 'form-control', 'placeholder' => 'Confirm']) !!}
            </div>

        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-9 pull-right">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>

        {{ csrf_field() }}
        {!! Form::close() !!}
    </div>
@endsection
