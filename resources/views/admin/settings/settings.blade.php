@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Template</h1>

      
    </div>
</div>

<div class="panel-body">
    <h2>Choose template</h2>
    <br>

    {!!
       Form::model($setting, ['route' => ['settings.update', $setting->id],
      'method' => 'put',
      'class' => 'form-horizontal']) !!}
    
        
        <div class="form-group">

        <label for="">Template 1
        <input type="radio" class="" {{$setting->flag == '1' ? 'checked': ''}} name="flag" value="1">
        <img src="/images/template-1.png" width="300" alt="">
        </label>
        </div>
        <div class="form-group">

        <label for="">Template 2
        <input type="radio" class="" {{$setting->flag == '2' ? 'checked': ''}} name="flag" value="2">
        <img src="/images/template-2.png" width="300" alt="">
        </label>
        </div>
        
        
        <input type="submit" class="btn btn-primary" value="Submit">
    </form>
</div>
@endsection
