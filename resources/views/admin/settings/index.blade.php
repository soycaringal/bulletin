@extends('layouts.admin')

@section('content')
<style>
    select {
  font-family: inherit;
}

label.img > input{ /* HIDE RADIO */
  visibility: hidden; /* Makes input not-clickable */
  position: absolute; /* Remove input from document flow */
}
label.img > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
}
label.img > input:checked + img{ /* (RADIO CHECKED) IMAGE STYLES */
  border:2px solid #f00;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Design Management</h1>

      
    </div>
</div>

<div class="panel-body">
    <h2>Choose template</h2>
    <br>

    {!!
       Form::model($template, ['route' => ['settings.update', $template->id],
      'method' => 'put',
      'class' => 'form-horizontal']) !!}

        <div class="form-group">
            <label for="">Template 1
            <input type="radio" class="" {{$template->flag == '1' ? 'checked': ''}} name="flag" value="1">
            <img src="/images/template-1.png" width="300" alt="">
            </label>
            </div>
            <div class="form-group">

            <labels>
                Template 2
                <input type="radio" class="" {{$template->flag == '2' ? 'checked': ''}} name="flag" value="2">
                <img src="/images/template-2.png" width="300" alt="">
            </label>
        </div>
        
        <input type="submit" class="btn btn-primary" value="Save">
    </form>

    <br>
    <hr>

    <div class="row">
        <div class="col-sm-5">
            
    {!!
       Form::model($bg, ['route' => ['settings.update', $bg->id],
      'method' => 'put',
      'class' => 'form-horizontal',
      'files' => true]) !!}
            

        <input type="hidden" name="flag" value="1">
        <label> <h2> Background </h2>
            Current Background
            <img src="/images/{{$bg->name}}" alt="" width="500" class="img-responsive">
            <br>
            
            Choose Existing Background
            <br>
            <label class="img">
              <input type="radio" name="default_image" value="bg.jpg" />
              <img src="/images/bg.jpg" width="140">
            </label>

            <label class="img">
              <input type="radio" name="default_image" value="template-3.jpg" />
              <img src="/images/template-3.jpg" width="140">
            </label>

            <label class="img">
              <input type="radio" name="default_image" value="template-4.jpeg" />
              <img src="/images/template-4.jpeg" width="140">
            </label>
            <br>
            Or Upload
            <br>
            <input class="form-control" name="bg_image" type="file">
        </label>

        <br>
        <br>
        <input type="submit" class="btn btn-primary" value="Save">
    </form>

        </div>
    </div>
    <hr>

    {!!
       Form::model($font, ['route' => ['settings.update', $font->id],
      'method' => 'put',
      'class' => 'form-horizontal',
      'files' => true]) !!}
        
        <input type="hidden" name="flag" value="1">
        <label><h2>Font Family</h2>

            <select name="font" id="" class="form-control">
                <option @if ($font->name == 'Arial') selected @endif value="Arial" style="font-family: Arial;">Arial</option>
                <option @if ($font->name == 'Helvetica') selected @endif value="Helvetica" style="font-family: Helvetica;">Helvetica</option>
                <option @if ($font->name == 'Times New Roman') selected @endif value="Times New Roman" style="font-family: Times New Roman;">Times New Roman</option>
                <option @if ($font->name == 'Times') selected @endif value="Times" style="font-family: Times;">Times</option>
                <option @if ($font->name == 'Courier New') selected @endif value="Courier New" style="font-family: Courier New;">Courier New</option>
                <option @if ($font->name == 'Courier') selected @endif value="Courier" style="font-family: Courier;">Courier</option>
                <option @if ($font->name == 'Verdana') selected @endif value="Verdana" style="font-family: Verdana;">Verdana</option>
                <option @if ($font->name == 'Georgia') selected @endif value="Georgia" style="font-family: Georgia;">Georgia</option>
                <option @if ($font->name == 'Palatino') selected @endif value="Palatino" style="font-family: Palatino;">Palatino</option>
                <option @if ($font->name == 'Garamond') selected @endif value="Garamond" style="font-family: Garamond;">Garamond</option>
                <option @if ($font->name == 'Bookman') selected @endif value="Bookman" style="font-family: Bookman;">Bookman</option>
                <option @if ($font->name == 'Comic Sans MS') selected @endif value="Comic Sans MS" style="font-family: Comic Sans MS;">Comic Sans MS</option>
                <option @if ($font->name == 'Trebuchet MS') selected @endif value="Trebuchet MS" style="font-family: Trebuchet MS;">Trebuchet MS</option>
                <option @if ($font->name == 'Arial Black') selected @endif value="Arial Black" style="font-family: Arial Black;">Arial Black</option>
                <option @if ($font->name == 'Impact') selected @endif value="Impact" style="font-family: Impact;">Impact</option>
            </select>
        </label>

        <br>
        <input type="submit" class="btn btn-primary" value="Save">
    </form>
</div>
@endsection
