<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ASIATECH CBRBS - Bulletin</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/sb-admin.css" rel="stylesheet">
    <link href="/css/summernote.css" rel="stylesheet">
    <link href="/assets/fonts/font-awesome.min.css" rel="stylesheet">
    <link href="/css/admin.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>


    <!-- Scripts -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src={{asset("/js/app.js")}}></script>
<script src={{asset("/js/metisMenu.min.js")}}></script>
<script src={{asset("/js/sb-admin.js")}}></script>
<script src={{asset("/js/summernote.min.js")}}></script>
<script src={{asset("/js/doon.js")}}></script>
<script src={{asset("/js/custom.js")}}></script>

</head>
<body>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ASIATECH CBRBS - Bulletin</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <a href="/"><i class="fa fa-sign-out fa-fw"></i> View Bulletin</a>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="#"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    
                    @if (Auth::user()->type == 'admin') 
                    <li>
                        <a href="/admin/users"><i class="fa fa-user"></i> Account Management</a>
                    </li>
                    @endif

                    <li>
                        <a href="/admin/posts"><i class="fa fa-table"></i> Post Management</a>
                        
                    </li>
                    <li data-toggle="collapse" data-target="#bulletin" class="collapsed">
                        <a href="#"><i class="fa fa-globe fa-lg"></i> Bulletin's Preview <span class="arrow"></span></a>
                    </li>  
                    <ul class="sub-menu collapse" id="bulletin">
                        @if (Auth::user()->type == 'admin') 
                        <li><a href="/admin/bulletin?type=faculty">Faculty</a></li>
                        <li><a href="/admin/bulletin?type=staff">Staff</a></li>
                        @endif
                        <li><a href="/admin/bulletin?type=student">Student</a></li>
                        @if (Auth::user()->type == 'admin') 
                        <li><a href="/admin/bulletin?type=general">General Bulletin</a></li>
                        @endif
                    </ul>
    
                     @if (Auth::user()->type == 'admin') 

                     <li>
                        <a href="/admin/settings"><i class="fa fa-table"></i> Design Management</a>
                        
                    </li>
                    @endif
                    @if (Auth::user()->type == 'admin') 

                    <li data-toggle="collapse" data-target="#report" class="collapsed">
                        <a href="#"><i class="fa fa-file fa-lg"></i> Report Management <span class="arrow"></span></a>
                    </li>  
                    <ul class="sub-menu collapse" id="report">
                        <li><a href="/admin/view?type=faculty">No. of Post Views</a></li>
                        <li><a href="/admin/rating">Ratings</a></li>
                    </ul>
                    @endif
                   
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        @yield('content')
    </div>
</div>


</body>
</html>
