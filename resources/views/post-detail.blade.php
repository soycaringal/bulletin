<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Bulletin</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">
        <link href="/css/sb-admin.css" rel="stylesheet">
        <link href="/assets/fonts/font-awesome.min.css" rel="stylesheet">
        <link href="/css/custom.css" rel="stylesheet">
        

        <script src="{{asset('js/angular.min.js')}}"></script>
        <script src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/custom.js')}}"></script>
        <script src="{{asset('js/doon.js')}}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">


<style>

    .template-1 {
        background: url('/images/{{$bg->name}}') no-repeat;
        background-size: cover;
        font-family: {{$font->name}};
    }
</style>
    </head>

    <body class="template template-{{ $template->flag}}">

        <h4><strong><a href="/" class=" pull-left" style="text-decoration: none; color: #fff">&nbsp; <i class="fa fa-home"></i> Home </a></strong></h4>
        <div class="container">
            <div class="row">
                <div class="col-sm-2"></div>
                    <div class="col-sm-8 col-md-8">
                        <div class="thumbnail">
                        @if (!$post->files->isEmpty())
                            <img width="100%" src="/images/{{ $post->files{0}->filename }}" alt=""></td>
                        @endif
                          <div class="caption">
                            <h3><a href="/post/{{ $post->id }}">{{ $post->title }}</a></h3>

                            
                                <div class="focus rating-group">
                                    <label class="pull-left"></label>
                                    <div class="rating-field" data-do="rating-field">
                                        @if ($aveRating == '0')
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($aveRating == 1)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($aveRating == 2)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($aveRating == 3)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($aveRating == 4)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @if ($aveRating == 5)
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        @endif
                                        <input type="hidden" name="rating" data-do="rating-vote" data-on="change" value="" />
                                        <input type="hidden" name="post_id" value="{{ $post->id}}" />
                                        <input type="hidden" name="profile_id" value="@if (isset(Auth::user()->id)){{ Auth::user()->id }}@endif">
                                    </div>
                                </div>
                               
                                
                            <p>{!! $post->desc !!}</p>
                            <i>{{ $post->created_at}}</i>
                          </div>
                        @if ($post->video)
                        <iframe width="100%" height="315" src="{{ $post->video }}" frameborder="0" allowfullscreen></iframe>
                        @endif
                        </div>
                    </div>

                
            </div>
          
        </div>
    </body>
</html>
