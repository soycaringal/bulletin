jQuery(function($) {
    /**
     * General Forms
     */
    (function() {


    /**
         * Tag Field
         */
        $(window).on('tag-field-init', function(e, target) {
            //translations
            try {
                var translations = JSON.parse($('#tag-translations').html());
            } catch(e) {
                var translations = {};
            }

            [
                'Tag'
            ].forEach(function(translation) {
                translations[translation] = translations[translation] || translation;
            });

            target = $(target);

            //TEMPLATES
            var tagTemplate = '<div class="tag"><input type="text" class="tag-input'
            + ' text-field" name="bulletin[]" placeholder="'
            + translations['Tag']+'" value="" />'
            + '<a class="remove" href="javascript:void(0)"><i class="fa fa-times">'
            + '</i></a></div>';

            var addResize = function(filter) {
                var input = $('input[type=text]', filter);

                input.keyup(function() {
                    var value = input.val() || input.attr('placeholder');

                    var test = $('<span>').append(value).css({
                        visibility: 'hidden',
                        position: 'absolute',
                        top: 0, left: 0
                    }).appendTo('header:first');

                    var width = test.width() + 10;

                    if((width + 40) > target.width()) {
                        width = target.width() - 40;
                    }

                    $(this).width(width);
                    test.remove();
                }).trigger('keyup');
            };

            var addRemove = function(filter) {
                $('a.remove', filter).click(function() {
                    var val = $('input', filter).val();

                    $(this).parent().remove();
                });
            };

            //INITITALIZERS
            var initTag = function(filter) {
                addRemove(filter);
                addResize(filter);

                $('input', filter).blur(function() {
                    //if no value
                    if(!$(this).val() || !$(this).val().length) {
                        //remove it
                        $(this).next().click();
                    }

                    var count = 0;
                    var currentTagValue = $(this).val();
                    $('div.tag input', target).each(function() {
                        if(currentTagValue === $(this).val()) {
                            count++;
                        }
                    });

                    if(count > 1) {
                        $(this).parent().remove();
                    }
                });
            };

            //EVENTS
            target.click(function(e) {
                if($(e.target).hasClass('tag-field')) {
                    var last = $('div.tag:last', this);

                    if(!last.length || $('input', last).val()) {
                        last = $(tagTemplate);
                        target.append(last);

                        initTag(last);
                    }

                    $('input', last).focus();
                }
            });

            //INITIALIZE
            $('div.tag', target).each(function() {
                initTag($(this));
            });
        });



    })();

    $(window).on('rating-field-init', function(e, target) {
        $('i', target).each(function(i) {
            $(this).click(function() {
                postId = $('input[name="post_id"]', target).val();
                profileId = $('input[name="profile_id"]', target).val();

                if(!profileId) {

                    toastr.error('You need to be logged in!')
                    setTimeout(function () { window.location = '/login'}, 2000);
                    return;
                }

                vote(i + 1, postId, profileId);

                $('i', target).each(function(j) {
                    if(j <= i) {
                        $(this)
                            .removeClass('fa-star-o')
                            .addClass('fa-star');

                        return;
                    }

                    $(this)
                        .removeClass('fa-star')
                        .addClass('fa-star-o');
                });

                $('input[name="rating"]', target).val(i + 1);
            });
        });
    });

    function vote(rating, postId, profileId) {
        
        data = {
            'rating': rating,
            'post_id': postId,
            'profile_id': profileId,
        };

        $.post('/api/rating/create', data, function(result) {

            if(result.error) {
                toastr.error(result.message)
                return;
            }

            toastr.success('Successfuly voted!')
        });           
    }


    //activate all scripts
    $(document.body).doon();
});

