<?php

use Illuminate\Database\Seeder;

use App\Post;
class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post1 = new Post();
        $post1->author = 'John Dela Cruz';
        $post1->title = 'We are pleased to announce the launch of our brand new website!';
        $post1->desc = 'After months of hard work, we are delighted to officially announce the launch of our website tomorrow November 11th, 2014. You can now find us at www.TownRealty.info.';
        $post1->video = 'https://www.youtube.com/embed/Bcv2cH8rsKU';
        $post1->type = 'Announcement';
        $post1->bulletin = json_encode(['staff', 'faculty']);
        $post1->flag = 1;
        $post1->save();

        $post2 = new Post();
        $post2->author = 'John Dela Cruz';
        $post2->title = 'Solar eclipse 2017: The pictures you have to see';
        $post2->desc = 'It was the first such eclipse to go from the west to east coasts of the US in 99 years.
From schoolchildren in Missouri to mounted patrol officers in Idaho, the nation was transfixed by the sight of the Moon drifting in front of the Sun, and blocking its light.';
        $post2->type = 'News';
        $post2->bulletin = json_encode(['staff', 'student']);
        $post2->flag = 1;
        $post2->save();

        $post3 = new Post();
        $post3->author = 'John Dela Cruz';
        $post3->title = 'Firefight near MSU in Marawi ends; school reopens';
        $post3->desc = 'Marawi City (CNN Philippines, August 22) — Two armed men were killed and four soldiers wounded on Tuesday in a two-hour firefight near the Mindanao State University (MSU) here, officials said.

The firefight in the town of Marantao began at around 5 a.m., and ended two hours later, the military\'s Joint Task Force Marawi said.';
        $post3->type = 'News';
        $post3->bulletin = json_encode(['staff']);
        $post3->flag = 1;
        $post3->save();
    }
}
