<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_admin = Role::where('name', 'admin')->first();
    	$role_staff = Role::where('name', 'staff')->first();

        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@bulletin.com';
        $admin->password = bcrypt('admin');
        $admin->employee_id = '1';
        $admin->type = 'admin';
        $admin->save();
        $admin->roles()->attach($role_admin);

        $staff = new User();
        $staff->name = 'Staff';
        $staff->email = 'staff@bulletin.com';
        $staff->password = bcrypt('staff');
        $staff->employee_id = '2';
        $staff->type = 'staff';
        $staff->save();
        $staff->roles()->attach($role_staff);
    }
}
