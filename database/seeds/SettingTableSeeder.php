<?php

use Illuminate\Database\Seeder;

use App\Setting;
class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new Setting();
        $setting->name = '';
        $setting->type = 'template';
        $setting->flag = '1';
        $setting->save();
    }
}
