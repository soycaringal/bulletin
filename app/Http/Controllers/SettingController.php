<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $template = Setting::where('type', 'template')->first();
        $bg = Setting::where('type', 'bg')->first();
        $font = Setting::where('type', 'font')->first();

        return view('admin.settings.index', ['template' => $template, 'bg' => $bg, 'font' => $font]);
    }

     public function bg()
    {
        $setting = Setting::where('type', 'bg')->first();
        return view('admin.settings.settings', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        $setting->flag = $request->flag;
        if(isset($request->font) && !empty($request->font)) {
            $setting->name = $request->font;
        }

        $file = $request->file('bg_image');
        if($request->hasFile('bg_image'))
        {
                $name = $file->getClientOriginalName();
                $file->move('images',  $name);
                $setting->name = $name;
        }
        if (isset($request->default_image) && !empty($request->default_image)) {
                $setting->name = $request->default_image;
        }
        
        $setting->save();

        return redirect(route('settings.index'));
    }
}
