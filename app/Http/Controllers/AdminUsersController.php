<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;

class AdminUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $users = new User();
        $input = $request->all();

        if(isset($input['q']) && !empty($input['q'])) {
            $users = $users->where('name', 'LIKE', "%" . $input['q'] . "%")
                ->orWhere('email', 'LIKE', "%" . $input['q'] . "%")
                ->orWhere('type', 'LIKE', "%" . $input['q'] . "%")->get();
        } else {
            $users = $users->all();
        }

        $users->current_user = app('Illuminate\Contracts\Auth\Guard')->user();
        
        return view('admin.users.index', compact('users'));
    }
    public function indexApi(Request $request)
    {

        $users = new User();
        $input = $request->all();

        if(isset($input['q']) && !empty($input['q'])) {
            $users = $users->where('name', 'LIKE', "%" . $input['q'] . "%")
                ->orWhere('email', 'LIKE', "%" . $input['q'] . "%")
                ->orWhere('type', 'LIKE', "%" . $input['q'] . "%")->get();
        } else {
            $users = $users->all();
        }

        $output = '';

        foreach ($users as $key => $user) {
            # code...

            $output .= '<tr>
            <td>'.$user->employee_id.'</td>
            <td>'.$user->name.'</td>
            <td>'.$user->alias.'</td>
            <td>'.$user->email.'</td>
            <td>'.$user->type.'</td>
            <td>
                <a href="/admin/users/'.$user->id.'/edit" class="btn btn-warning">Edit</a>

                <form method="POST" action="/admin/users/'.$user->id.'" accept-charset="UTF-8" style="display:inline"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="MrpHB4A1iB07js8EG9Kh9HKXOLTaWXThMsa2lMN0">
                <input class="btn btn-danger" type="submit" value="Delete">
                </form>
            </td>
        </tr>';

        }

        return Response($output);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $role = Role::where('name', 'staff')->first();
        $user = new User();
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->alias = $input['alias'];
        $user->type = $input['type'];
        $user->employee_id = $input['employee_id'];
        $user->password = bcrypt($input['password']);

        $user->save();
        $user->roles()->attach($role);

        return redirect(route('admin'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
       
        $user->name = $request->name;
        $user->alias = $request->alias;
        $user->type = $request->type;
        $user->employee_id = $request->employee_id;
        
        $user->save();
        return redirect(route('admin'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect(route('admin'));
    }
}
