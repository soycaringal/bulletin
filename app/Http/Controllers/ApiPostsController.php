<?php

namespace App\Http\Controllers;

use App\Post;
use App\Rating;
use App\Setting;
use App\View;
use Illuminate\Http\Request;

class ApiPostsController extends Controller
{
    
    public function index()
    {

        $filter = [];

        $filter['flag'] = 1; 

        $posts = Post::where($filter);
        if(isset(\Auth::user()->type) && \Auth::user()->type != 'admin') {
            $posts->where('bulletin', 'like', '%'.\Auth::user()->type.'%');
        }

        $posts = $posts->get();
        // foreach ($posts as $key => $value) {
        //     $posts[$key]['files'] = $value->files;

        //     $ratings = Rating::where('post_id', $value->id)->get();

        //     $aveRating = 0;

        //     if(!$ratings->isEmpty()) {
        //         $rate = [
        //             5 => 0,
        //             4 => 0,
        //             3 => 0,
        //             2 => 0,
        //             1 => 0
        //             ];
        //         foreach ($ratings as $key => $value) {
        //             $rate[$value['rating']]++; 
        //         }


        //         $totalWeight = 0;
        //         $totalReviews = 0;

        //         foreach ($rate as $weight => $numberofReviews) {
        //             $WeightMultipliedByNumber = $weight * $numberofReviews;
        //             $totalWeight += $WeightMultipliedByNumber;
        //             $totalReviews += $numberofReviews;
        //         }

        //         //divide the total weight by total number of reviews
        //         $aveRating = $totalWeight / $totalReviews;
        //     }

        //     $posts[$key]['aveRating'] = $aveRating;

        //     $authId = null;
        //     if(isset(\Auth::user()->id)) {
        //         $authId = \Auth::user()->id;
        //     } 

        //     $posts[$key]['authId'] = $authId;
        // }
        
        

        $posts['template'] = Setting::where('type', 'template')->first();

        return response()->json(compact('posts'));
    }

    public function views()
    {
        $views = View::all();
        
        $data = [];
        foreach($views as $key => $view) {
            $data[$key][] = strtotime($view['created_at']) * 1000;
            $data[$key][] = (int) date('h', strtotime($view['created_at']));
        }

        return response()->json($data);
    }
    public function template(){
        $template = Setting::where('type', 'template')->first();

        return response()->json(compact('template'));
    }

    public function rating(Request $request)
    {
        $input = $request->all();

        $exists = Rating::
            where('post_id', $input['post_id'])
            ->where('profile_id', $input['profile_id'])->count();

        if($exists) {
            return response()->json(['message' => 'Already voted!' ,'error' => true]);

        } 

        $rating = new Rating();
        $rating->rating = $input['rating'];
        $rating->post_id = $input['post_id'];
        $rating->profile_id = $input['profile_id'];

        $rating->save();

        return response()->json(compact('rating'));
    }

     public function preview()
    {

        $filter = [];

        $filter['flag'] = 1; 

        $posts = Post::where($filter);
        if(isset($_GET['type']) && ($_GET['type'] != 'all')) {
            $posts->where('bulletin', 'like', '%'.$_GET['type'].'%');
        }

        $posts = $posts->get();


        $posts['template'] = Setting::where('type', 'template')->first();

        return response()->json(compact('posts'));
    }

}
