<?php

namespace App\Http\Controllers;

use App\Rating;
use App\Post;
use App\File;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset($_GET['type'])) {
            $posts = Post::where('bulletin', 'like', '%'.$_GET['type'].'%')->get();
        } else if(isset($_GET['q']) && !empty($_GET['q'])) { 
            $posts = new Post();

            $posts = $posts->where('author', 'LIKE', "%" . $_GET['q'] . "%")
                    ->orWhere('title', 'LIKE', "%" . $_GET['q'] . "%")
                    ->orWhere('type', 'LIKE', "%" . $_GET['q'] . "%")
                    ->orWhere('bulletin', 'LIKE', "%" . $_GET['q'] . "%")
                    ->orWhere('desc', 'LIKE', "%" . $_GET['q'] . "%")->get();
        } else {
            $posts = Post::orderBy('id', 'DESC')->get();
        }

        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {   


        if(isset($_GET['q']) && !empty($_GET['q'])) { 
            $posts = new Post();
            $posts = $posts->where('author', 'LIKE', "%" . $_GET['q'] . "%")
                    ->orWhere('title', 'LIKE', "%" . $_GET['q'] . "%")
                    ->orWhere('type', 'LIKE', "%" . $_GET['q'] . "%")
                    ->orWhere('bulletin', 'LIKE', "%" . $_GET['q'] . "%")
                    ->orWhere('desc', 'LIKE', "%" . $_GET['q'] . "%")->get();
        } else {
            $posts = Post::all();
        }

        foreach ($posts as $key => $value) {
            $posts[$key]['view'] = \DB::table('views')
            ->where('post_id', $value['id'])
            ->get()->count();
        }

        $charts = \DB::select('select count(*) as total, year(created_at) as year from views group by year(created_at)');



        foreach ($charts as $key => $chart) {
            $data[$chart->year]['year'] = $chart->year;
            $data[$chart->year]['total'] = $chart->total;
        }


        for ($i = min($data)['year']; $i <= 2022; $i++) {
            if(!isset($data[$i])) {
                $data[$i]['total'] = 0;
                $data[$i]['year'] = $i;
            }
        }
        
        // dd($data);
        return view('admin.posts.view', [
            'posts' => $posts, 
            'charts' => $data, 
            'startYear' => min($data)
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function rating()
    {   

        $filter['flag'] = 1; 

        $posts = Post::where($filter);
        // if(isset(\Auth::user()->type) && \Auth::user()->type != 'admin') {
        //     $posts->where('bulletin', 'like', '%'.\Auth::user()->type.'%');
        // }

        $posts = $posts->get();

        foreach ($posts as $key => $post) {
            if(isset($post->files[0]->filename)) {
                $posts[$key]['filename'] = $post->files[0]->filename;
            } else {
                $posts[$key]['filename'] = '';
            }

            $ratings = Rating::where('post_id', $post->id)->get();

            $aveRating = 0;

            if(!$ratings->isEmpty()) {
                $rate = [
                    5 => 0,
                    4 => 0,
                    3 => 0,
                    2 => 0,
                    1 => 0
                    ];
                foreach ($ratings as $value) {
                    $rate[$value['rating']]++; 
                }


                $totalWeight = 0;
                $totalReviews = 0;

                foreach ($rate as $weight => $numberofReviews) {
                    $WeightMultipliedByNumber = $weight * $numberofReviews;
                    $totalWeight += $WeightMultipliedByNumber;
                    $totalReviews += $numberofReviews;
                }

                //divide the total weight by total number of reviews
                $aveRating = $totalWeight / $totalReviews;

                $aveRating = floor($aveRating);
                // dd($aveRating);
            }

            $posts[$key]['aveRating'] = $aveRating;

             $authId = null;
            // if(isset(Auth::user()->id)) {
            //     $authId = Auth::user()->id;
            // } 

            $posts[$key]['authId'] = $authId;

            $limit = 250;

             $string = strip_tags($post['desc']);

            if (strlen($string) > $limit) {
                // truncate string
                $stringCut = substr($string, 0, $limit);

                // make sure it ends in a word so assassinate doesn't become ass...
                $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="/post/'.$post['id'].'">Read More</a>';
            }

            $posts[$key]['desc'] = $string;

        }

        $rating = [];
        foreach ($posts as $key => $value) {

            if ($value['aveRating'] == 1) {
                $rating[1] = isset($rating[1]) ? $rating[1] + 1 : 1;
            }

            if ($value['aveRating'] == 2) {
                $rating[2] = isset($rating[2]) ? $rating[2] + 1 : 1;
            }

            if ($value['aveRating'] == 3) {
                $rating[3] = isset($rating[3]) ? $rating[3] + 1 : 1;
            }

            if ($value['aveRating'] == 4) {
                $rating[4] = isset($rating[4]) ? $rating[4] + 1 : 1;
            }

            if ($value['aveRating'] == 5) {
                $rating[5] = isset($rating[5]) ? $rating[5] + 1 : 1;
            }
        }

        if (!isset($rating[1])) {
            $rating[1] = 0;
        }

        if (!isset($rating[2])) {
            $rating[2] = 0;
        }

        if (!isset($rating[3])) {
            $rating[3] = 0;
        }

        if (!isset($rating[4])) {
            $rating[4] = 0;
        }

        if (!isset($rating[5])) {
            $rating[5] = 0;
        }

        // if(isset($_GET['q']) && !empty($_GET['q'])) { 
        //     $posts = new Post();
        //     $posts = $posts->where('author', 'LIKE', "%" . $_GET['q'] . "%")
        //             ->orWhere('title', 'LIKE', "%" . $_GET['q'] . "%")
        //             ->orWhere('type', 'LIKE', "%" . $_GET['q'] . "%")
        //             ->orWhere('bulletin', 'LIKE', "%" . $_GET['q'] . "%")
        //             ->orWhere('desc', 'LIKE', "%" . $_GET['q'] . "%")->get();
        // } else {
        //     $posts = Post::all();
        // }

        // foreach ($posts as $key => $value) {
        //     $posts[$key]['view'] = \DB::table('views')
        //     ->where('post_id', $value['id'])
        //     ->get()->count();
        // }

        // $charts = \DB::select('select count(*) as total, year(created_at) as year from views group by year(created_at)');



        // foreach ($charts as $key => $chart) {
        //     $data[$chart->year]['year'] = $chart->year;
        //     $data[$chart->year]['total'] = $chart->total;
        // }


        // for ($i = min($data)['year']; $i <= 2022; $i++) {
        //     if(!isset($data[$i])) {
        //         $data[$i]['total'] = 0;
        //         $data[$i]['year'] = $i;
        //     }
        // }
        
        return view('admin.posts.rating', [
            'posts' => $posts, 
            'rating' => $rating, 
            // 'startYear' => min($data)
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bulletin()
    {   
        if(isset($_GET['type'])) {
            $posts = Post::where('bulletin', 'like', '%'.$_GET['type'].'%')
            ->orderBy('id', 'DESC')->get();
        } else if(isset($_GET['q']) && !empty($_GET['q'])) { 
            $posts = new Post();

            $posts = $posts->where('author', 'LIKE', "%" . $_GET['q'] . "%")
                    ->orWhere('title', 'LIKE', "%" . $_GET['q'] . "%")
                    ->orWhere('type', 'LIKE', "%" . $_GET['q'] . "%")
                    ->orWhere('bulletin', 'LIKE', "%" . $_GET['q'] . "%")
                    ->orWhere('desc', 'LIKE', "%" . $_GET['q'] . "%")->get();
        } else {
            $posts = Post::orderBy('id', 'DESC')->get();
        }

        return view('admin.posts.bulletin', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = [
            'name' => 'news', 
            'name' => 'post'
        ];
        return view('admin.posts.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $post = new Post();
        $post->author = $input['author'];
        $post->title = $input['title'];
        $post->desc = $input['desc'];
        $post->bulletin = json_encode($input['bulletin']);
        $post->type = $input['type'];
        $post->video = !empty($input['video']) ? $this->getYoutubeEmbedUrl($input['video']) : '';

        $post->save();

        $files = $request->file('files');

        if($request->hasFile('files'))
        {
            foreach ($files as $file) {
                $name = $file->getClientOriginalName();
                $file->move('images',  $name);

                $post->files()->save(new File(['filename' => $name]));
            }
        }

        return redirect(route('posts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $post->bulletin = json_decode($post->bulletin);
        return view('admin.posts.edit', compact('post'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        if(isset($request->flag)) {
            $post->flag = $request->flag;
        } else {
            $post->author = $request->author;
            $post->title = $request->title;
            $post->desc = $request->desc;
            $post->bulletin = json_encode($request->bulletin);
            $post->type = $request->type;    
            $post->video = !empty($request->video) ? $this->getYoutubeEmbedUrl($request->video) : '';

        }
        
        $post->save();

        $files = $request->file('files');

        if($request->hasFile('files'))
        {
            foreach ($files as $file) {
                $name = $file->getClientOriginalName();
                $file->move('images',  $name);

                $post->files()->save(new File(['filename' => $name]));
            }
        }

        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect(route('posts.index'));
    }


    /**
     * Get Youtube Embed Url
     *
     */
    public function getYoutubeEmbedUrl($url)
    {
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
        $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

        if (preg_match($longUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }

        if (preg_match($shortUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
        return 'https://www.youtube.com/embed/' . $youtube_id ;
    }
}
