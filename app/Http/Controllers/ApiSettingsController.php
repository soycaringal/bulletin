<?php

namespace App\Http\Controllers;

use App\Post;
use App\Setting;
use App\View;
use Illuminate\Http\Request;

class ApiSettingsController extends Controller
{
    
    public function index()
    {

        $filter = [];

        $filter['flag'] = 1; 

        $posts = Post::where($filter);
        if(isset(\Auth::user()->type) && \Auth::user()->type != 'admin') {
            $posts->where('bulletin', 'like', '%'.\Auth::user()->type.'%');
        }

        $posts = $posts->get();

        foreach ($posts as $key => $value) {
            $posts[$key]['files'] = $value->files;
        } 

        $posts['template'] = Setting::where('type', 'template')->first();
        
        return response()->json(compact('posts'));
    }

    public function views()
    {
        $views = View::all();
        
        $data = [];
        foreach($views as $key => $view) {
            $data[$key][] = strtotime($view['created_at']) * 1000;
            $data[$key][] = (int) date('h', strtotime($view['created_at']));
        }

        return response()->json($data);
    }

       public function preview()
    {

        $filter = [];

        $filter['flag'] = 1; 

        $posts = Post::where($filter);
        if(isset(\Auth::user()->type) && \Auth::user()->type != 'admin') {
            $posts->where('bulletin', 'like', '%'.\Auth::user()->type.'%');
        }

        $posts = $posts->get();

        foreach ($posts as $key => $value) {
            $posts[$key]['files'] = $value->files;
        } 

        $posts['template'] = Setting::where('type', 'template')->first();
        
        return response()->json(compact('posts'));
    }

}
